$(window).ready(function(){
   	$('#import-bigbuy').click(function(e){
   		$(this).prop("disabled",true);
     	var ajaxes  = [
            {
                url      : BaseUrl + 'products/import_cats_from_big_buy'
            },
            {
                url      : BaseUrl + 'products/import_products_from_big_buy'
            },
            {
                url      : BaseUrl + 'products/import_bg_products_images'
            },
            {
                url      : BaseUrl + 'products/import_bg_products_informations'
            },
            {
                url      : BaseUrl + 'products/import_bg_products_stocks'
            }
        ],
        current = 0;
     function do_ajax() {
        if (current < ajaxes.length) {
            $.ajax({
                url      : ajaxes[current].url,
				type: "GET",
				dataType: "JSON",
                success  : function (response) {
                	if(response.hasOwnProperty('message')){
                	 $('#target-import').append('<p>' + response.message + '</p>');
                	}
                },
                complete : function (response) {
                    current++;
                    do_ajax();

                }
            });
        }
    }
    do_ajax();            
	});
	// export woocommerce
   	$('#export-woocomerce').click(function(e){
   		$(this).prop("disabled",true);
     	var woo  = [
            {
                url      : BaseUrl + 'products/export_woo_categories'
            },
            {
                url      : BaseUrl + 'products/export_woo_products'
            },
            {
                url      : BaseUrl + 'products/export_woo_images'
            },            
            {
                url      : BaseUrl + 'products/order_woo_categories'
            }
        ],
        current = 0;
     function woo_ajax() {
        if (current < woo.length) {
            $.ajax({
                url      : woo[current].url,
				type: "GET",
				dataType: "JSON",
                success  : function (response) {
                	if(response.hasOwnProperty('message')){
                	 $('#target-import').append('<p>' + response.message + '</p>');
                	}
                },
                complete : function (response) {
                    current++;
                    woo_ajax();

                }
            });
        }
    }
    woo_ajax();	
	});
	// update bigbuy
   	$('#update-biguy').click(function(e){
		$.ajax({
			url: BaseUrl + 'products/update_big_buy_stocks',
			type: "GET",
			dataType: "JSON"
		}).done(function (response){
			$('#target-import').append('<p>' + response.message + '</p>');
		  	}); 		
	});
	// update woocommerce
   	$('#update-woocommerce').click(function(e){
		$.ajax({
			url: BaseUrl + 'products/update_woo_products',
			type: "GET",
			dataType: "JSON"
		}).done(function (response){
			$('#target-import').append('<p>' + response.message + '</p>');
		  	}); 		
	});	
});

// functions
function previewFile(d) {
  var parent = $(".preview");
  var file    = d.files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
  	console.log(parent);
  	$(parent).html('<img class="preview" src="'+reader.result+'">');
  }, false);

  if (file) {
    reader.readAsDataURL(file);
    // empty
    $(parent).html('');
  }
}

function readURL(d) {
	var f = $(d).parents(".js-image-group")[0];
	var g = $(f).children(".row")[0];
	var e = $(".js-image-box", f);
	var c = $(f).data("gal-fk");
	var b = $(d).data("input");
	if (b !== "gal_image") {
		$(g).html("")
	}
	if (d.files && d.files[0]) {
		var a = new FileReader();
		a.onload = function (i) {
			$(g).append('<div class="js-image-col col-xs-6 col-sm-4 col-md-3 col-lg-2">	<input type="hidden" class="js-image-url" name="' + b + '" value="' + d.files[0].name + '">	<div class="file-uploaded-images">		<div class="image js-image-box">			<input type="hidden" class="js-image-id" data-image-name="' + d.files[0].name + '" name="' + b + '_id[]" value="0">			<figure class="delete-image"><i class="fa fa-close"></i></figure>			<div class="thumb-display" style="background-image:url(\'' + i.target.result + '\');" alt="">			</div>		</div>	</div></div>');
			if (b === "gal_image") {
				var h = new FormData();
				h.append("gal_image", d.files[0].name);
				h.append("upload_gal_image", d.files[0]);
				h.append("galleries_fk", c);
				$.ajax({
					url: BaseUrl + "images/add/",
					type: "POST",
					dataType: "json",
					data: h,
					processData: false,
					contentType: false,
					cache: false,
					async: true
				}).done(function (j) {
					if (!j.status) {
						alert("Problemi durante il caricamento dell'immagine");
						return false
					}
					$('input[data-image-name="' + d.files[0].name + '"]', f).val(j.id)
				}).fail(function () {
					alert("Impossibile caricare l'immagine")
				})
			}
		};
		a.readAsDataURL(d.files[0])
	}
}
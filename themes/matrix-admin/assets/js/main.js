$(document).ready(function() {
	// invio login
	$('form[name="loginform"]').submit(function(e){
		e.preventDefault();
		var credentials = $(this).serializeArray();
		var action = $(this).attr("action");
		sendAuthAjax(action, credentials, '#auth-target');
	});
	$('form[name="registerform"]').submit(function(e){
		e.preventDefault();
		var credentials = $(this).serializeArray();
		var action = $(this).attr("action");
		sendAuthAjax(action, credentials);	
	});
	$('form[name="lostpassword"]').submit(function(e){
		e.preventDefault();
		var credentials = $(this).serializeArray();
		var action = $(this).attr("action");
		sendAuthAjax(action, credentials, '#message');	
	});
	// send add activity
	$('form[name="addactivity"]').submit(function(e){
		e.preventDefault();
		var credentials = $(this).serializeArray();
		credentials.push({name: 'description', value: quill.container.firstChild.innerHTML});
		var action = $(this).attr("action");
		sendAuthAjax(action, credentials, '#message');	
	});		
})

function sendAuthAjax(action, credentials, field = false){
	$.ajax({
			url: action,
			type: "POST",
			dataType: "JSON",
			data: credentials,
		}).done(function (response) {
			if (response.status) {
					$(field).html('<div class="padding-20">'+response.message+'</div>');
					$("html, body").animate({
					scrollTop: $("#animate-target").offset().top
				}, 2000, function() {
    									$(location).attr('href', BaseUrl);
 									 })
			}
			else {
				$(field).html(response.message)
			}
		}).fail(function (f, d, g) {
		   	alert(f.responseText);
			});
}
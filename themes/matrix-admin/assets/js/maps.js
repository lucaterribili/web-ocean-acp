$(document).ready(function() {
 	autocomplete();
 // fix autocomplete blur
 	$("#address").blur(function (){
        //if(!$(".pac-container").is(":focus") && !$(".pac-container").is(":visible"))
            selectFirstResult();
	});
});
// functions
function autocomplete(){
    var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    	var place = autocomplete.getPlace();
        $('#lat').val(place.geometry.location.lat());
        $('#lon').val(place.geometry.location.lng());
         });	
}

function selectFirstResult() {
	//infowindow.close();
	$(".pac-container").hide();
	var firstResult = $(".pac-container .pac-item:first").text();
	
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({"address":firstResult }, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	        var lat = results[0].geometry.location.lat(),
	            lng = results[0].geometry.location.lng(),
	            placeName = results[0].address_components[0].long_name,
	            latlng = new google.maps.LatLng(lat, lng);
	            string = results[0].formatted_address.replace(", Italia", "");

	        //moveMarker(placeName, latlng);
	        $("#address").val(string);
	        $('#lat').val(lat);
	        $('#lon').val(lng);
	        // move maps
	    }
	});   
}
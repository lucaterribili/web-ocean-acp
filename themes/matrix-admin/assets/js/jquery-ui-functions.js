// AUTOCOMPLETE RESTAURANT GROUP
$("#item_title").autocomplete({
	minLength: 3,
	delay: 1400,
	source: function (f, a) {
		var d = $(this.element);
		var e = d.val();
		var b = BaseUrl + "weboceanitems/suggest/" + e;
		var c = d.data("jqXHR");
		if (c) {
			c.abort()
		}
		d.data("jqXHR", $.ajax({
			url: b,
			type: "get",
			dataType: "json",
			success: function (g) {
				if (g.status == true) {
					a($.map(g.data, function (h) {
						return {
							label: h.title,
							value: h.id
						}
					}))
				}
				if (g.status == false) {}
			}
		}))
	},
	fail: function (a, b) {
	 	console.log(a, b)
	},
	focus: function (a, b) {
		a.preventDefault();
		$("#item_title").val(b.item.label)
	},
	select: function (b, c) {
		b.preventDefault();
		$("#item_title").val(c.item.label);
		$("#item_fk").val(c.item.value);
		return false
	}
})
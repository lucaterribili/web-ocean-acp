        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 id="animate-target" class="page-title">Form Basic</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="message"></div>
                <form name="addactivity" action="<?= BASE_URL;?>items/send_add" class="form-horizontal" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Form Elements</h5>
                                <div class="form-group m-t-20">
                                    <label>title</label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="title_here">
                                </div>
                                <div class="form-group">
                                    <label>subcatdesc</label>
                                    <input type="text" class="form-control" name="subcatdesc" id="subcatdesc" placeholder="subcatdesc_here">
                                </div>
                                <div class="form-group">
                                    <label>short_description</label>
                                    <input type="text" class="form-control" id="short_description" placeholder="short_description_here">
                                </div>
                                <div class="form-group">
                                    <label>vat</label>
                                    <input type="text" class="form-control" name="pv" id="pv" placeholder="Enter Phone Number">
                                </div>
                                <div class="form-group">
                                    <label>email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Enter Purchase Order">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Form Elements</h5>
                                <div class="form-group m-t-20">
                                    <label>address</label>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Enter Date">
                                </div>
                                <div class="form-group">
                                    <label>city</label>
                                    <input type="text" class="form-control" name="city" id="city" placeholder="Enter Phone Number">
                                </div>
                                <div class="form-group">
                                    <label>zip</label>
                                    <input type="text" class="form-control" name="zip" id="zip" placeholder="International Phone Number">
                                </div>
                                <div class="form-group">
                                    <label>region</label>
                                    <input type="text" class="form-control" name="pv" id="pv" placeholder="Enter Phone Number">
                                </div>
                                <div class="form-group">
                                    <label>phone</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter Purchase Order">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- editor -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Quill Editor</h4>
                                <!-- Create the editor container -->
                                <div id="editor" name="editor" style="height: 300px;">
                                <textarea name="text" style="display:none" id="hiddenArea"></textarea>
                                </div>
                            </div><!-- card body -->
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type="hidden" name="lat" id="lat">
                                        <input type="hidden" name="lon" id="lon">
                                        <input type="hidden" name="macroid" id="macroid">
                                        <input type="hidden" name="microid" id="microid">
                                        <input type="hidden" name="user_fk" id="user_fk">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- start buttons -->
                <!-- end  buttons -->
                </form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
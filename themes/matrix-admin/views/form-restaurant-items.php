        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 id="animate-target" class="page-title">Form Basic</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div id="message"></div>
                <form name="addmenu" action="<?= BASE_URL;?>restaurants/send_add_items" class="form-horizontal" method="post" enctype="multipart/form-data"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title m-b-0">Form Elements</h5>
                                <div class="form-group m-t-20">
                                    <label>restaurant_name</label>
                                    <input type="text" class="form-control" name="item_title" id="item_title" placeholder="restaurant_name_here">
                                </div>
                                <div class="form-group m-t-20">
                                    <label>restaurant_group</label>
                                    <select name="rg_fk" class="form-control">
                                    <?php foreach ($group as $items) {
                                        echo '<option value="'.$items->id.'">'.$items->label.'</option>';
                                    }
                                    ?> 
                                    </select>
                                </div>
                                <!-- / group -->
                                <div class="form-group m-t-20">
                                    <label>title</label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="title_here">
                                </div> 
                                <!-- / title -->
                                <div class="form-group m-t-20">
                                 <label>media</label>
                                <div class="file-upload">

                                        <div class="MultiFile-wrap" id="MultiFile1">
                                            <div class="preview"></div>
                                            <input type="file" id="media" data-input="media" name="media" class="file-upload-input with-preview MultiFile-applied" title="Clicca per allegare un'immagine" maxlength="10" accept="gif|jpg|png" value="" onchange="previewFile(this);">
                                        </div>
                                        <span>Clicca o trascina qui l'immagine</span>
                                    </div>
                                </div> 
                                <!-- / title -->                                      <div class="form-group m-t-20">
                                    <label>description</label>
                                    <input type="text" class="form-control" name="description" id="description" placeholder="description_here">
                                </div>
                                <!-- / description -->
                                <div class="form-group m-t-20">
                                    <label>price</label>
                                    <input type="number" step="0.01" class="form-control" name="price" id="price" placeholder="title_here">
                                </div>
                                <!-- / price -->                                                             
                            </div>
                        </div>
                    </div>
                </div>
                <!-- editor -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <input type="hidden" name="item_fk" id="item_fk">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- start buttons -->
                <!-- end  buttons -->
                </form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
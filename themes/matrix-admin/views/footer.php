          <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= $theme_url; ?>libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= $theme_url; ?>libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= $theme_url; ?>libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= $theme_url; ?>libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?= $theme_url; ?>extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?= $theme_url; ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= $theme_url; ?>js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= $theme_url; ?>js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
<?php 
    switch ($uri) {
        case 'index':
        case 'dashboard':
            echo '<script src="'.$theme_url.'libs/flot/excanvas.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot/jquery.flot.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot/jquery.flot.pie.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot/jquery.flot.time.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot/jquery.flot.stack.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot/jquery.flot.crosshair.js"></script>';
            echo '<script src="'.$theme_url.'libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>';
            echo '<script src="'.$theme_url.'js/pages/chart/chart-page-init.js"></script>';
            echo '<script src="'.$theme_url.'js/ecommerce.js"></script>';
        break;
        case 'list':
            echo '<script src="'.$theme_url.'extra-libs/multicheck/datatable-checkbox-init.js"></script>';
            echo '<script src="'.$theme_url.'extra-libs/multicheck/jquery.multicheck.js"></script>';
            echo '<script src="'.$theme_url.'extra-libs/DataTables/datatables.min.js"></script>';
            echo '<script src="'.$theme_url.'js/list.js"></script>';
            echo '<script>$(\'#zero_config\').DataTable({"pageLength": 200});</script>';
        break;
        case 'simple-form':
            echo '<script src="'.$theme_url.'js/images.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-ui/jquery-ui.min.js"></script>';
            echo '<script src="'.$theme_url.'js/jquery-ui-functions.js"></script>';            
        break;
        case 'form':
        case 'edit-form':
            echo '<script src="'.$theme_url.'libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>';
            echo '<script src="'.$theme_url.'js/pages/mask/mask.init.js"></script>';
            echo '<script src="'.$theme_url.'libs/select2/dist/js/select2.full.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/select2/dist/js/select2.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-asColor/dist/jquery-asColor.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-asGradient/dist/jquery-asGradient.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-minicolors/jquery.minicolors.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>';
            echo '<script src="'.$theme_url.'libs/jquery-ui/jquery-ui.min.js"></script>';
            echo '<script src="'.$theme_url.'js/jquery-ui-functions.js"></script>';
            echo '<script src="'.$theme_url.'libs/quill/dist/quill.min.js"></script>';
            echo '<script>
                  //***********************************//
                  // For select 2
                  //***********************************//
                  $(".select2").select2();

                  /*colorpicker*/
                  $(\'.demo\').each(function() {
                  //
                  // Dear reader, it\'s actually very easy to initialize MiniColors. For example:
                  //
                  //  $(selector).minicolors();
                  //
                  // The way I\'ve done it below is just for the demo, so don\'t get confused
                  // by it. Also, data- attributes aren\'t supported at this time...they\'re
                  // only used for this demo.
                  //
                  $(this).minicolors({
                          control: $(this).attr(\'data-control\') || \'hue\',
                          position: $(this).attr(\'data-position\') || \'bottom left\',

                          change: function(value, opacity) {
                              if (!value) return;
                              if (opacity) value += \', \' + opacity;
                              if (typeof console === \'object\') {
                                  console.log(value);
                              }
                          },
                          theme: \'bootstrap\'
                      });

                  });
                  /*datwpicker*/
                  jQuery(\'.mydatepicker\').datepicker();
                  jQuery(\'#datepicker-autoclose\').datepicker({
                      autoclose: true,
                      todayHighlight: true
                  });
                  var quill = new Quill(\'#editor\', {
                      theme: \'snow\'
                  });

                 </script>';
            echo '<script src="'.$theme_url.'js/main.js"></script>';
            echo '<script type="text/javascript" src="https://maps.google.com/maps/api/js?key='.$gm_api_key.'&libraries=places&language=it&region=it"></script>';
            echo '<script src="'.$theme_url.'js/maps.js"></script>';
            echo '<script src="'.$theme_url.'js/images.js"></script>';
            break;
                }
?>
</body>

</html>
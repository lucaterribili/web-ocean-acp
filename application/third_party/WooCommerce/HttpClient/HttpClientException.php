<?php
/**
 * WooCommerce REST API HTTP Client Exception
 *
 * @category HttpClient
 * @package  Automattic/WooCommerce
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'third_party/WooCommerce/HttpClient/Request.php';
require_once APPPATH.'third_party/WooCommerce/HttpClient/Response.php';
/**
 * REST API HTTP Client Exception class.
 *
 * @package Automattic/WooCommerce
 */
class HttpClientException extends \Exception
{
    /**
     * Request.
     *
     * @var Request
     */
    private $request;

    /**
     * Response.
     *
     * @var Response
     */
    private $response;

    /**
     * Initialize exception.
     *
     * @param string   $message  Error message.
     * @param int      $code     Error code.
     * @param Request  $request  Request data.
     * @param Response $response Response data.
     */
    public function __construct($message, $code, Request $request, Response $response)
    {
        parent::__construct($message, $code);

        $this->request  = $request;
        $this->response = $response;
    }

    /**
     * Get request data.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Get response data.
     *
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}

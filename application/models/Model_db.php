<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_db extends MY_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_in($select_field, $field, $data, $table)

	{
     
     $this->db->select($select_field);

     $this->db->where_in($field, $data);

     $query = $this->db->get($table);

     return $query->result();

	}

}

/* End of file Model_db.php */
/* Location: ./application/models/Model_db.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_extra_profile extends MY_Model {

	private $primary_key 	= 'AU_id';
	private $table_name 	= 'user_profile_codguadagno_view';
	private $field_search 	= ['AU_id', 'email', 'pass', 'username', 'full_name', 'avatar', 'banned', 'step_fk', 'sex', 'date_birth', 'city','address', 'cap', 'telephone', 'codiceguadagno', 'firstname', 'lastname', 'date_created', 'referral_user_fk'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function extra_change($id, $data = array())

	{

     $sql = "SELECT * FROM aauth_users_profile WHERE user_fk = " . $id;

     $query = $this->db->query($sql);

     if($query->num_rows() == 0)

     {

      // inserisco dato in tabella profilo

      $data['user_fk'] = $id;

      $data['from_app_fk'] = 1;

      $data['sex'] = 0;

      $this->db->insert('aauth_users_profile', $data);
      
      return $this->db->affected_rows();

     }

     else

     {
     
      // aggiorno il dato nella tabella profilo

      $this->db->where('user_fk', $id);
      
      $this->db->update('aauth_users_profile', $data);

      return $this->db->affected_rows();
     	
     }
	
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "user_profile_codguadagno_view.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		//$this->join_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('user_profile_codguadagno_view.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function join_avaiable() {
		$this->db->join('apps', 'apps.id = user_profile_codguadagno_view.from_app_fk', 'LEFT');
	    
    	return $this;
	}

}

/* End of file Model_user_profile_codguadagno_view.php */
/* Location: ./application/models/Model_user_profile_codguadagno_view.php */
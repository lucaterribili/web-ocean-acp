<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller

{
    protected $global = array();

    public $user;
	
    function __construct()
	{
	 parent::__construct();

        $this->user = $this->account->get_user();

        $this->global = [
            'app_fk'            => $this->config->item('app_fk'),
            'base_url'          => BASE_URL,
            'gm_api_key'        => $this->config->item('google_maps_key'),
            'theme_url'         => theme_url($this->config->item('site_theme'),'assets'),
            'user'              => $this->user
            ];
    }

}
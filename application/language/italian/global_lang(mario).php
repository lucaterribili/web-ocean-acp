<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// 404 //
$lang['error'] = 'Errore';
$lang['404_page_not_found'] = '404 - Pagina non trovata';
$lang['404_error_msg'] = 'La pagina che stai cercando non esiste!';

// HEADER - MENU SX
$lang['prices'] = 'Prezzi';
$lang['contacts'] = 'Contatti';
$lang['blog'] = 'Blog';

// HEADER - MENU DX
$lang['register'] = 'Registrati';
$lang['login'] = 'Entra';
$lang['forgot_password'] = 'Ho dimenticato la password';

// MODAL SIGN-IN / REGISTER / FORGOT PASSWORD
$lang['login_title'] = 'Autenticazione';
$lang['register_title'] = 'Registrazione';
$lang['forgot_password_title'] = 'Reimposta Password';

$lang['forgot_password_msg'] = 'Per richiedere una nuova password inserisci l\'email che hai utilizzato in fase di registrazione. Riceverai una email da YourIndex con le istruzioni per reimpostarla.';
$lang['forgot_password_bt'] = 'Continua';

// USER DATA
$lang['name'] = 'Nome';
$lang['first_name'] = 'Nome';
$lang['last_name'] = 'Cognome';
$lang['gender'] = 'Genere';
$lang['birthday'] = 'Data di nascita';
$lang['address'] = 'Indirizzo';
$lang['zip_code'] = 'C.A.P.';
$lang['city'] = 'Città';
$lang['email'] = 'Email';
$lang['vat'] = 'Partita Iva';
$lang['phone'] = 'Telefono';
$lang['password'] = 'Password';
$lang['old_password'] = 'Vecchia Password';
$lang['new_password'] = 'Nuova Password';
$lang['digit_old_password'] = 'Digita la tua vecchia password';
$lang['digit_new_password'] = 'Digita la tua nuova password';
$lang['confirm_password'] = 'Conferma Password';
$lang['confirm_new_password'] = 'Conferma la tua nuova password';
$lang['accept_policy'] = 'Ho preso visione dell\'informativa sulla privacy';
$lang['update_profile'] = 'Aggiorna profilo';
$lang['upload_avatar'] = 'Carica avatar';
$lang['change_password'] = 'Cambia password';
$lang['submit'] = 'Invia';
$lang['register_now'] = 'Registrati';
$lang['accept_terms_after_registation'] = 'Cliccando sul bottone "Registrati" accetti';
$lang['site_options'] = 'Opzioni sito';
$lang['domain'] = 'Dominio';
$lang['show_types'] = 'Attiva sezioni sito';
$lang['select_template'] = 'Seleziona template';
$lang['news'] = 'News';
$lang['prices'] = 'Prezzi';
$lang['price'] = 'Prezzo';
$lang['price_list'] = 'Listino prezzi';
$lang['service_list'] = 'Lista servizi';
$lang['menu_list'] = 'Menu ristorante';
$lang['blog'] = 'Blog';
$lang['event'] = 'Eventi';
$lang['list'] = 'Lista';
$lang['offers'] = 'Offerte';
$lang['title'] = 'Titolo';

// HEADER - MENU DX - LOGGED
$lang['edit_profile'] = 'Modifica profilo';
$lang['change_password'] = 'Cambia password';
$lang['add_your_business'] = 'Aggiungi la tua attività';
$lang['sign_out'] = 'Esci';

// FOOTER
$lang['copyright_long'] = 'YourIndex. Tutti i diritti riservati';
$lang['privacy_label'] = 'Informativa sulla privacy';
$lang['terms_conditions'] = 'Termini e Condizioni';

// HOMEPAGE - FORM SEARCH
$lang['what_placeholder'] = 'Cosa stai cercando?';
$lang['locality_placeholder'] = 'Inserisci una località';
$lang['my_position'] = 'La mia posizione attuale';
$lang['your_position'] = 'La tua posizione attuale';
$lang['search_better'] = 'Affina ricerca';
$lang['search_in_title'] = 'Cerca nel titolo';
$lang['search_in_cats'] = 'Cerca nelle categorie';
$lang['search_in_services'] = 'Cerca nei servizi';

// HOMEPAGE - MAP SX + SEARCH RESULTS DX
$lang['new_search'] = 'Nuova ricerca';
$lang['search_found_results'] = 'Risultati trovati: <span class="results-number">%s</span>';
$lang['search_results'] = 'Risultati della ricerca';
$lang['search_no_results'] = 'Nessun risultato trovato';

// DASHBOARD
$lang['expand_all'] = 'Espandi';
$lang['collapse_all'] = 'Condensa';
$lang['about_this_listing'] = 'Descrizione';
$lang['reviews']  = 'Recensioni';
$lang['features'] = 'Servizi'; 
$lang['blog_posts'] = 'Novità di questa attività';
$lang['meal'] = 'Menu del ristorante';
$lang['social_share'] = 'Condividi';
$lang['write_a_review'] = 'Lascia una recensione';
$lang['activity_title'] = 'Scheda attività di %s';
$lang['activity_edit']  = 'Modifica';
$lang['activity_delete'] = 'Cancella scheda';
$lang['website_view'] = 'Visita sito';
$lang['website_off'] = 'Sospendi sito';
$lang['website_on'] = 'Attiva sito';
$lang['dashboard_assign_user'] = 'Utente';
$lang['dashboard_edit_sheet'] = 'Scheda';
$lang['dashboard_edit_website'] = 'Sito';
$lang['dashboard_edit_contacts'] = 'Contatti';
$lang['dashboard_edit_gallery'] = 'Galleria';
$lang['dashboard_edit_post'] = 'Posts';

// DASHBOARD - ADD/EDIT
$lang['add_business_title'] = 'Inserimento nuova attività';
$lang['edit_business_title'] = 'Modifica attività';
$lang['my_business'] = 'Le mie attività';
$lang['restaurant_menu'] = 'Menu ristorante';
$lang['section'] = 'Sezione';
$lang['activity_logo'] = 'Immagine Logo (primo piano Homepage)';
$lang['about_image'] = 'Immagine About (sezione Chi Siamo)';
$lang['header_image'] = 'Immagine Header (sfondo Homepage)';
$lang['click_or_drag_here'] = 'Clicca o trascina qui l\'immagine';
$lang['add_other_meal'] = 'Aggiungi un\'altra portata';

$lang['business_profile_section'] = 'Profilo';
$lang['business_contact_section'] = 'Contatti';
$lang['business_website_template_section'] = 'Selezione template';

$lang['search_user_by_fullname_email'] = 'Cerca l\'utente per nome, cognome e email';

$lang['add_business'] = 'Aggiungi scheda';
$lang['website_exists'] = 'Sito web già assegnato all\'attività selezionata';
$lang['business_name'] = 'Nome attività';
$lang['subcategory'] = 'Categoria attività';
$lang['title_business'] = 'Titolo scheda';
$lang['short_description'] = 'Descrizione breve';
$lang['long_description'] = 'Descrizione lunga'; 
$lang['description'] = 'Description'; 
$lang['contacts_title'] = 'Contatti';
$lang['address'] = 'Indirizzo';
$lang['address_mask'] = 'Daje tutta';
$lang['city'] = 'Città';
$lang['pv'] = 'Provincia';
$lang['zip'] = 'Cap';
$lang['Telefono'] = 'Phone (non inserire spazi)';

// DASHBOARD POST - DELETE
$lang['confirm_post_delete'] = 'Clicca <b>Conferma</b> per cancellare il post selezionato';
$lang['delete_post'] = 'Elimina post';
$lang['cancel'] = 'Annulla';
$lang['confirm'] = 'Conferma';

// DASHBOARD POST - ADD
$lang['problems_adding_post'] = 'Attenzione. Problemi nell\'inserimento del nuovo post.';
$lang['post_added_successfully'] = 'Post aggiunto con successo.';
$lang['select_menu_type'] = 'Seleziona tipologia di menu';
$lang['add_new_dish'] = 'Aggiungi una nuova portata';
$lang['add_post'] = 'Aggiungi Post';

// DASHBOARD POST - EDIT UPDATE
$lang['edit_post'] = 'Modifica Post';
$lang['update_post'] = 'Aggiorna Post';
$lang['problems_updating_post'] = 'Attenzione. Problemi nell\'aggiornamento del nuovo post.';
$lang['post_updated_successfully'] = 'Post aggiornato con successo.';

// DASHBOARD - USERS
$lang['add_user'] = 'Aggiungi utente';
$lang['create_user'] = 'Crea utente';
$lang['load_registered_user'] = 'Carica utente registrato';
$lang['load_user'] = 'Carica utente';
$lang['assign_user'] = 'Assegna utente';
$lang['register_user_and_set_owner'] = 'Registra utente ed assegna attività';
$lang['compile_all_required_fields'] = 'Attenzione, compilare tutti i campi necessari';

// DASHBOARD SITE
$lang['domain_preview'] = 'Anteprima dominio';

// DASHBOARD SITE - TYPES
$lang['gallery'] = 'Galleria';
$lang['contact'] = 'Contatti';
$lang['post'] = 'Post';
$lang['menu'] = 'Menu';
$lang['services'] = 'Servizi';
$lang['events'] = 'Eventi';
// POST FIELDS
$lang['event_date'] = 'Data evento';
$lang['event_address'] = 'Indirizzo evento';
// BLOG //
$lang['read_more'] = 'Leggi tutto';

// BREADCRUMB //
$lang['home'] = 'Home';
$lang['activities'] = 'Ricerca attività';

// PRIVACY //
$lang['privacy_msg'] = 'Questo sito fa uso di cookie per migliorare l’esperienza di navigazione degli utenti e per raccogliere informazioni sull’utilizzo del sito stesso. Utilizziamo sia cookie tecnici sia cookie di parti terze per inviare messaggi promozionali sulla base dei comportamenti degli utenti. Può conoscere i dettagli consultando la nostra privacy policy. Proseguendo nella navigazione si accetta l’uso dei cookie; in caso contrario è possibile abbandonare il sito.';
$lang['privacy_read_more'] = 'Leggi informativa estesa';

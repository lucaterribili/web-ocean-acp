<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ************** SIDEBAR **********
****************************************
********************************************** */

$lang['items'] = 'Attività';
$lang['items_display'] = 'Visualizza tutte le attività';
$lang['items_add'] = 'Aggiungi Attività';
$lang['users'] = 'Utenti';
$lang['posts'] = 'Posts';

// START CUSTOM LANGUAGE SECTION
$lang['users_one'] = 'Utenti WebOcean';
$lang['users_two'] = 'Utenti OstiaTv';
$lang['restaurants'] = 'Ristoranti';
$lang['users_two'] = 'Utenti OstiaTv';
$lang['kw_categories'] = 'Categorie KiwiCommerce';
$lang['kw_categories_all'] = 'Lista Categorie';
$lang['kw_categories_import'] = 'Importa e rigenera';
$lang['products'] = 'Negozio elettronico';
$lang['products_dashboard'] = 'Wizard';
$lang['products_all_products'] = 'Tutti i prodotti';
$lang['products_all_categories'] = 'Tutte le categorie';
// END CUSTOM LANGUAGE SECTION
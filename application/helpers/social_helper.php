<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Helper get_og()
 * ------------------------------------------------------------------------
 * @access    public
 * @param     array(
 *				title,
 *				url,
 *				image,
 *				description
 * )
 * @return    string
 */
if (!function_exists('get_og'))
{
	function get_og($data = [])
	{
		$ci =& get_instance();

		$data['image'] = isset($data['image']) ?
			$data['image'] :
			$ci->config->item('static_repo').'social/og-thumb.png';

		$data['url'] = isset($data['url']) ?
			$data['url'] :
			BASE_URL;

		$data['title'] = isset($data['title']) ?
		$data['title'] :
		$ci->config->item('site_metatitle');

		$data['description'] = isset($data['description']) ?
			$data['description'] :
			$ci->config->item('site_description');

		ob_start();

		// VIEW ============== ?>
		<meta property="og:title" content="<?= $data['title'] ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?= $data['url'] ?>" />
		<meta property="og:image" content="<?= $data['image'] ?>" />
		<meta property="og:description" content="<?= $data['description'] ?>" />
		<?php // =============

		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
}


/**
 * Helper echo_social_share()
 * ------------------------------------------------------------------------
 * @access	public
 * @param 	array(
 *				title,
 *				url,
 *				image,
 *				description
 * 			)
 * @return	string
 */
if (!function_exists('echo_social_share'))
{
	function echo_social_share($data = [])
	{
		$ci =& get_instance();

		$data['image'] = isset($data['image']) ?
			$data['image'] :
			$ci->config->item('static_repo').'social/og-thumb.png';

		$data['url'] = isset($data['url']) ?
			$data['url'] :
			current_url();

		$data['title'] = isset($data['title']) ?
		$data['title'] :
		 'Yourindex.red';
		$data['description'] = isset($data['description']) ?
			$data['description'] :
			$ci->config->item('site_description');

		ob_start();

		// VIEW ============== ?>
		<div class="post_share_icons_wrapper">
			<span class="social_share_item_wrapper">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?=current_url();?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-facebook fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://twitter.com/share?url=<?=urlencode(current_url());?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-twitter fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://plus.google.com/share?url=<?=current_url();?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-google-plus fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=current_url();?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-linkedin fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://www.pinterest.com/pin/create/button/?url=<?=current_url();?>&media=<?=$data['image'];?>&description=<?=urlencode($data['description']);?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-pinterest fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://reddit.com/submit?url=<?=current_url();?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-reddit fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://www.tumblr.com/share/link?url=<?=current_url();?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
				<i class="fa fa-tumblr fa-lg"></i>
				</a>
			</span>
			<span class="social_share_item_wrapper">
				<a href="https://getpocket.com/save?url=<?=current_url();?>&title=<?=urlencode($data['description']);?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" rel="nofollow" target="_blank">
					<i class="fa fa-get-pocket fa-lg"></i>
				</a>
			</span>
		</div>
		<?php // =============

		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
} 


/**
 * Helper get_social_buttons()
 * ------------------------------------------------------------------------
 * @access    public
 * @param     array(
 * )
 * @return    string
 */
if (!function_exists('get_social_buttons'))
{
	function get_social_buttons($data, $metatitle = '', $postId = 0)
	{
		$ci =& get_instance();
		//$ci->config->item('site_description');

		if ($postId === 0)
			$pageUrl = current_url();
		else
			$pageUrl = base_url('post/'.$postId);

		$pageUrl = urlencode($pageUrl);
		$url = $ci->config->item('social_'.$data->title);
		$url = str_replace('[URL]', $pageUrl, $url);
		$url = str_replace('[TITLE]', $metatitle, $url);

		ob_start();
		// VIEW ============== ?>
		<div class="jssocials-share jssocials-share-<?= $data->title ?>">
			<a target="_blank" href="<?= $url ?>" class="jssocials-share-link">
				<i class="<?= $data->iconfa ?> jssocials-share-logo"></i>
			</a>
			<div class="jssocials-share-count-box jssocials-share-no-count">
				<span class="jssocials-share-count"></span>
			</div>
		</div>
		<?php // =============
		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}
} 


/**
 * Helper print_social_buttons()
 * ------------------------------------------------------------------------
 * @access    public
 * @param     array(
 * )
 * @return    string
 */
if (!function_exists('print_social_buttons'))
{
	function print_social_buttons($data, $metatitle, $postId = 0)
	{
		echo get_social_buttons($data, $metatitle, $postId);
	}
} 





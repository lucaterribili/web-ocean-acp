<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Helper asset_url()
 * ------------------------------------------------------------------------
 * @access    public
 * @param    string
 * @return    string
 */
if ( ! function_exists('asset'))
{
    function asset($sitename)
    {
        $_ci =& get_instance();
        return $_ci->config->base_url('views/themes/'.$sitename);
    }
} 
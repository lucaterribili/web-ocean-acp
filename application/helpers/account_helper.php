<?php
if(!function_exists('check_login')) {
	function check_login() {
		$ci =& get_instance();

		if (!$ci->account->is_loggedin() && $ci->uri->segment(2) != 'login')
			redirect('auth/login');
	}
}

if(!function_exists('check_admin')) {
	function check_admin() {
		$ci =& get_instance();
		
		if ($ci->account->im_admin() == FALSE && $ci->uri->segment(2) != 'not_allowed')
			redirect('auth/not_allowed');
	}
}
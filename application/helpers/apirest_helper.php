<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* NUOVE API */
if (!function_exists('api_login_user')) {
	function api_login_user($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'login';

		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key')], true, $data));
		return $result;
	}
}
if (!function_exists('api_users_webocean')) {	
	function api_users_webocean($filter = null, $field = null,$limit=5) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'users/all';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Authorization: Bearer ' . $ci->user->token]));
	}
}
// Restaurants
if (!function_exists('api_restaurants_group_display')) {
	function api_restaurants_group_display() {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'restaurants/dishes/all';

		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
		return $result;
	}
}

if (!function_exists('api_restaurants_items_display')) {
	function api_restaurants_items_display() {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'restaurants/menu/all';

		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
		return $result;
	}
}

if (!function_exists('api_restaurants_group_add')) {	
	function api_restaurants_group_add($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'restaurants/dishes/add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Authorization: Bearer ' . $ci->user->token], TRUE, $data));
	}
}

if (!function_exists('api_restaurants_items_add')) {	
	function api_restaurants_items_add($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'restaurants/menu/add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Authorization: Bearer ' . $ci->user->token], TRUE, $data));
	}
}
// ITEMS
if (!function_exists('api_webocean_items_suggest')) {	
	function api_webocean_items_suggest($like) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'items/suggest?like='.$like;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Authorization: Bearer ' . $ci->user->token]));
	}
}
/* END NUOVE API */
// User
if (!function_exists('api_user_all')) {	
	function api_user_all($filter = null, $field = null,$limit=5) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'user/all?filter=' . $filter . '&field='.$field.'&limit='. $limit;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_aauth_profile')) {	
	function api_aauth_profile($user_fk) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'aauth_users_profile/detail?user_fk=' . $user_fk;
		
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_user_verification')) {	
	function api_user_verification($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'user/verify_user';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')], TRUE, $data));
	}
}

if (!function_exists('api_update_profile')) {	
	function api_update_profile($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'user/update_profile';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_update_password')) {	
	function api_update_password($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'user/change_password';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

// Autocomplete //

if (!function_exists('api_autcomplete_activity_subcategory_list')) {	
	function api_autcomplete_activity_subcategory_list($filter, $limit=5) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'autocomplete/all?filter=' . $filter . '&limit='. $limit;
		
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

// Sub Category To Group //

if (!function_exists('api_get_subcategory_to_group_list')) {	
	function api_get_subcategory_to_group_list($filter='', $field='', $selfields='catid,subcatid,catdesc,subcatdesc,iconfa,score') {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'subcategories/all?filter=' . $filter . '&field='. $field . '&fieldsel=' . $selfields;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_get_category_to_group_list')) {	
	function api_get_category_to_group_list($filter='', $field='', $selfields='id,catdesc,sort,iconfa,min_score', $sort = 'catdesc', $sortdir = 'ASC') {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'categories/all?filter=' . $filter . '&field='. $field . '&fieldsel=' . $selfields . '&sort=' . $sort . '&sort=ASC';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

// Data Category //

if (!function_exists('api_get_subcategory_list')) {	
	function api_get_subcategory_list($filter, $field='pcatdesc', $limit=10, $selfields='id,pcatid,pcatdesc,score') {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_categories/suggest?filter=' . $filter . '&field=' . $field . '&limit='. $limit . '&fieldsel=' . $selfields;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

// Activity //

if (!function_exists('api_delete_activity')) {
	function api_delete_activity($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/remove';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], TRUE, $data));
	}
}

if (!function_exists('api_delete_bulk_activity')) {
	function api_delete_bulk_activity($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/bulk_remove';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], TRUE, $data));
	}
}

if (!function_exists('api_suggest_activity')) {
	function api_suggest_activity($filter, $field='title', $limit=10, $selfields='id,title,type,address,city,zip,pv,lat,lon,subcatdesc,subcatid,email,phone') {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/suggest?filter=' . $filter . '&field=' . $field . '&limit='. $limit . '&fieldsel=' . $selfields;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_get_activity_detail')) {	
	function api_get_activity_detail($dp_fk) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/detail?id=' . $dp_fk;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_get_activity_list_by_user')) {
	function api_get_activity_list_by_user($start=1, $limit=10) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/all_by_user?start=' . $start . '&limit=' . $limit;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_search_for_activities')) {
	function api_search_for_activities($filter, $pcatid, $lat, $lon, $distance=15, $delimiter = 1, $limit=0, $start=1) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/extended_search?filter=' . $filter . '&pcatid=' . $pcatid . '&lat=' . substr($lat, 0, 6) . '&lon=' . substr($lon, 0, 6) . '&distance=' . $distance. '&delimiter=' . $delimiter . '&limit='. $limit . '&start=' . $start;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_all_activities')) {
	function api_all_activities($filter = null, $field = null, $limit = 0) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/all_world?filter=' . $filter .'&field='.$field.'&limit='.$limit;

		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_search_for_nearby_activities')) {
	function api_search_for_nearby_activities($pcatid, $lat, $lon, $distance=30, $dp_fk) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/search?pcatid=' . $pcatid . '&lat=' . substr($lat, 0, 6) . '&lon=' . substr($lon, 0, 6) . '&distance=' . $distance . '&limit=5&exclude=' . $dp_fk;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_update_activity')) {
	function api_update_activity($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/update';
		//$api = 'http://resteh.local/api/data_providers/update'; // <------------------- TEST MODE (TO BE DELETED)
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_enable_post_website')) {
	function api_enable_post_website($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/enable_post_website';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_generate_activity')) {
	function api_generate_activity($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/generate';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_set_activity_owner')) {
	function api_set_activity_owner($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/set_data_provider_owner';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_register_user_and_set_data_provider_owner')) {
	function api_register_user_and_set_data_provider_owner($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/register_user_and_set_data_provider_owner';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}		

// Posts //

if (!function_exists('api_get_post_list')) {
	function api_get_post_list($dp_fk) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/all?field=dp_fk&filter=' . $dp_fk;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_get_post_detail')) {
	function api_get_post_detail($id) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/detail?id=' . $id;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_get_post_menu')) {
	function api_get_post_menu($dp_fk) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/check_restaurant_menu?dp_fk=' . $dp_fk;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' .$ci->user->token]));
	}
}

if (!function_exists('api_get_post_type_list')) {
	function api_get_post_type_list() {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'post_type/all';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_get_post_group')) {
	function api_get_post_group() {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'post_group/all?fields=title,id';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' .$ci->user->token]));
	}
}

if (!function_exists('api_get_post_group_title')) {
	function api_get_post_group_title($id) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'post_group/detail?id=' . $id . '&fields=title';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_add_post')) {
	function api_add_post($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_update_post')) {
	function api_update_post($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/update';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_publish_post')) {
	function api_publish_post($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/publish';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_delete_post')) {
	function api_delete_post($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/delete';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_delete_post_thumb')) {
	function api_delete_post_thumb($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'posts/delete_thumb';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

// Gallery //

if (!function_exists('api_images_gallery_by_dp')) {
	function api_images_gallery_by_dp($filter) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'images/gallery_by_dp?dp_fk=' . $filter;

		//$api = 'http://resteh.local/api/images/gallery_by_dp?dp_fk=' . $filter; // <------------------- TEST MODE (TO BE DELETED)
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_images_regenerate_thumb')) {
	function api_images_regenerate_thumb($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'images/regenerate_thumb';

		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], TRUE, $data));
	}
}

if (!function_exists('api_images_all_by_gallery')) {
	function api_images_all_by_gallery($filter) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'images/all_by_gallery?galleries_fk=' . $filter;
		//$api = 'http://resteh.local/api/images/all_by_gallery?galleries_fk=' . $filter; // <------------------- TEST MODE (TO BE DELETED)
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_images_delete')) {
	function api_images_delete($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'images/delete';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_images_add')) {
	function api_images_add($data) {
		$ci =& get_instance();

		$api = $ci->config->item('api_gateway').'images/add';
		//$api = 'http://resteh.local/api/images/add'; // <------------------- TEST MODE (TO BE DELETED)
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

// Templates //

if (!function_exists('api_get_template_list')) {
	function api_get_template_list() {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'templates/all?filter=1&field=active';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' .$ci->user->token]));
	}
}

if (!function_exists('api_get_template_by_cat_list')) {
	function api_get_template_by_cat_list($filter, $field) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'templates/all?filter=' . $filter . '&field='.$field;
		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_get_template_detail')) {
	function api_get_template_detail($template_id) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'templates/detail?id=' . $template_id;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

// Websites //

if (!function_exists('api_add_website')) {
	function api_add_website($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'websites/add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_check_website')) {
	function api_check_website($id) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/detail?id=' . $id;
		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
		return $result->data->website;
	}
}

if (!function_exists('api_get_number_user_activty')) {
	function api_get_number_user_activty($user_id) {
		if($user_id == 0)return 0;
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'data_providers/all?filter=' . $user_id . '&field=user_fk';
		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
		return $result->total;
	}
}

// User //

if (!function_exists('api_suggest_user')) {
	function api_suggest_user($filter) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'user/suggest?filter='.$filter;
		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
		return $result;
	}
}

if (!function_exists('api_login_user')) {
	function api_login_user($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'login';

		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key')], true, $data));
		return $result;
	}
}

if (!function_exists('api_confirm_new_password')) {
	function api_confirm_new_password($data) {
		$ci =& get_instance();
		
		$api = $ci->config->item('api_gateway').'user/confirm_new_password';

		$result = json_decode(rest_server($api, [$ci->config->item('x_api_key')], true, $data));

		return $result;
	}
}

################ API FOR BIG_BUY TABLES #######################

// Categories

if (!function_exists('api_import_bg_categories')) {	
	function api_import_bg_categories() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_categories/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_import_bg_categories_tech')) {	
	function api_import_bg_categories_tech() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_categories/fill_tech';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_import_bg_categories_kitchen')) {	
	function api_import_bg_categories_kitchen() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_categories/fill_kitchen';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_main_bg_categories')) {	
	function api_main_bg_categories($limit = 0) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_categories/main?limit=' .$limit;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

// Products

if (!function_exists('api_import_bg_products')) {	
	function api_import_bg_products() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_import_bg_products_tech')) {	
	function api_import_bg_products_tech() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_tech/bulk_fill';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_all_bg_products')) {	
	function api_all_bg_products($limit = 0) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products/main?limit=' . $limit;
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

// Products images

if (!function_exists('api_import_bg_products_images')) {	
	function api_import_bg_products_images() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products_images/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

// Products informations

if (!function_exists('api_import_bg_products_informations')) {	
	function api_import_bg_products_informations() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products_informations/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

// Products stocks

if (!function_exists('api_import_bg_products_stocks')) {	
	function api_import_bg_products_stocks() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products_stocks/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_update_bg_products_stocks')) {	
	function api_update_bg_products_stocks() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products_stocks/bulk_update';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

// Products attributes

if (!function_exists('api_import_bg_products_attributes')) {	
	function api_import_bg_products_attributes() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'big_buy_products_attributes/bulk_add';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

################ API WOOCOMMERCE #######################

if (!function_exists('api_export_woo_categories')) {	
	function api_export_woo_categories() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/import_categories';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_export_woo_products')) {	
	function api_export_woo_products() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/import_products';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_export_woo_images')) {	
	function api_export_woo_images() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/add_images';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_export_extra_fields')) {	
	function api_export_extra_fields() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/import_extra_fields';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_export_woo_brands')) {	
	function api_export_woo_brands() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/add_attribute_brand';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('export_woo_products_tags')) {	
	function export_woo_products_tags() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/import_products_tags';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_order_woo_categories')) {	
	function api_order_woo_categories() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/order_categories';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token]));
	}
}

if (!function_exists('api_update_woo_products')) {	
	function api_update_woo_products() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/update_products';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_manage_stock_woo_store')) {	
	function api_manage_stock_woo_store() {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/manage_stock';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key')]));
	}
}

if (!function_exists('api_update_woo_product')) {	
	function api_update_woo_product($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/update_product';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}

if (!function_exists('api_update_woo_category')) {	
	function api_update_woo_category($data) {
		$ci =& get_instance();
		$api = $ci->config->item('api_gateway').'woocommerce_tools/update_category';
		return json_decode(rest_server($api, [$ci->config->item('x_api_key'), 'X-Token: ' . $ci->user->token], true, $data));
	}
}
<?php

if (!function_exists('theme_url')) {
	function theme_url($template, $url_additional = null) {
		$ci =& get_instance();
		return BASE_URL . 'themes/' . $template . ((!is_null($url_additional)) ? '/' . $url_additional : '') . '/';
	}
}

if (!function_exists('rest_server')) {
function rest_server($server, $headers = false, $post = false, $data = null, $file = null, $debug = FALSE)
{

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $server);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, false);

	if ($headers) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	if ($post and (is_array($data) and count($data))) {
		curl_setopt($ch, CURLOPT_POST, $post);
		curl_setopt($ch, CURLOPT_POSTFIELDS, (http_build_query($data)));
	}

	if ($file) {
		//curl_setopt($ch, CURLOPT_URL, $file);
	}

	$result = curl_exec($ch);

	curl_close($ch);

	if($debug == TRUE){
		var_dump($result);
		exit();
	}

	return $result;
}
}

if (!function_exists('line_replace')) {
	function line_replace($categories, $array){
		$new_arr = [];
    	foreach ($categories as $items) {
    		if(isset($items['ID'])){
    	    		var_dump($items['URL_REWRITTEN']);
    		exit();		
    		}

    		if(in_array($items['ID'], $array))
    		{
    		 array_push($new_arr, $items['NAME']);
    		}
    	}
     return $new_arr;
	}
}
function build_post_fields( $data,$existingKeys='',&$returnArray=[]){
    if(($data instanceof CURLFile) or !(is_array($data) or is_object($data))){
        $returnArray[$existingKeys]=$data;
        return $returnArray;
    }
    else{
        foreach ($data as $key => $item) {
            build_post_fields($item,$existingKeys?$existingKeys."[$key]":$key,$returnArray);
        }
        return $returnArray;
    }
}
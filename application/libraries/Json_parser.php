<?php

/**
 * CC app Class
 *
 * @package			EHsystem  
 * @category		component
 */

class Json_parser
{
 
 public function __construct()
 
 {
   
  $this->ci =& get_instance();
  
  $this->ci->load->database();

  $this->ci->load->model('model_db');
 
 }

 public function parser($json)

 {

  $engine = [];

  $schema = json_decode($json);
  
  $ids = (array)$schema->datasrc;

  $data = $this->get_providers($ids);

  // sostituisce i dati nella mapppa

  $output = $this->intersecate($data, $schema->modules);

  $engine['data'] = $output['data'];

  unset($output['data']);

  $engine['modules'] = $this->get_modules($output);

  // return template engine

  return $engine;

 }

 public function get_providers($ids)

 {
  
  $d = [];

  $raw = $this->ci->model_db->get_in(['provider', 'json'], 'id', $ids, 'data_providers');

  foreach($raw as $item)

  {
  
   $d[$item->provider] = json_decode($item->json);

  }

  return $d;

 }

 public function intersecate($data, $map)

 {

  $output = [];

  $pattern = '#\<((tfk|pg|cst)\.([a-z]+))\>#';

  $i = 0;

  foreach ($map as $m)
  {
   $map['data'][$m->name] = preg_replace_callback($pattern,
               function ($matches) use($data) {
                return $this->find_replace($matches, $data);
                },  
               (array)$m->data);
   unset($map[$i]->data);
   $i++;
  }
  
  return $map;

 }

 public function find_replace($matches, $data)
 {

  if($matches[3] == 'tfk') $o = $data[$matches[2]]->$matches[3];
  
  else 
  {
  
   $o = 'Dato di pagine gialle';
  
  }
  
  return $o;
 }

 public function get_menu($source)
 {



 }

 public function get_images($gallery)
 {



 }

 public function get_modules($source)

 {
 
  $html = $sort = array();

  // cycle data to get active modules

  $result = array_filter($source, function($item) {
  
   return $item->show == "true";
  
  });

  uasort($result, function($item1, $item2){
    
   return $item1->sort > $item2->sort;

  });

  return $result;

 }

}	
<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}


class Account

{

	/**
	 * Constructor
	 */

	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('session');
	}

	public function set_jwt_session($data, $token)
	{
		$this->CI->load->helper('string');

		// Remove cookies first

		$cookie = array(
			'name'   => 'user',
			'value'  => '',
			'expire' => -3600,
			'path'   => '/',
		);

		$this->CI->input->set_cookie($cookie);

		// store in sessione

		$user_data = array(
			'id'		=>	$data->user_id,
			'username'	=>	$data->username,
			'loggedin'	=>	true,
			'role'      => 	$data->role,
			'token'		=>	$token,
			'data'		=>	$data
		);

		$this->CI->session->set_userdata($user_data);

		// creo cookie

		$random_string = random_string('alnum', 16);

		$cookie = array(
			'name'   => 'user',
			'value'  => $data->user_id . "-" . $random_string,
			'expire' => 99 * 999 * 999,
			'path'   => '/',
		);

		$this->CI->input->set_cookie($cookie);

	}

	public function get_user()
	{
		$user_id = $this->CI->session->userdata('id');
		$loggedin = $this->is_loggedin($user_id);

		return (object)[
						'id'  => $user_id,
						'loggedin' => $loggedin,
						'role'     => $this->CI->session->userdata('role'),
						'token'    => $this->CI->session->userdata('token')
						];
	}

	public function is_loggedin($user_id = false){
		if ($user_id == false) {
			$user_id = $this->CI->session->userdata('id');
		}

		$loggedin = is_null($user_id) ? FALSE : TRUE;

		return $loggedin;
	}

	public function im_admin() {
		return $this->CI->session->userdata('role') == 1;
	}

	public function logout(){

        $cookie = array(
            'name'   => 'user',
            'value'  => '',
            'expire' => -3600,
            'path'   => '/',
        );
        $this->CI->input->set_cookie($cookie);

        return $this->CI->session->sess_destroy();
    }

} // end class

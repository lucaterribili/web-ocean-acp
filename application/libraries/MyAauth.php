<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Aauth is a User Authorization Library for CodeIgniter 2.x, which aims to make 
 * easy some essential jobs such as login, permissions and access operations. 
 * Despite ease of use, it has also very advanced features like private messages, 
 * groupping, access management, public access etc..
 *
 * @author		Emre Akay <emreakayfb@hotmail.com>
 * @contributor Jacob Tomlinson
 * @contributor Tim Swagger (Renowne, LLC) <tim@renowne.com>
 * @contributor Raphael Jackstadt <info@rejack.de>
 *
 * @copyright 2014-2016 Emre Akay
 *
 * @version 2.5.11
 *
 * @license LGPL
 * @license http://opensource.org/licenses/LGPL-3.0 Lesser GNU Public License
 *
 * The latest version of Aauth can be obtained from:
 * https://github.com/emreakay/CodeIgniter-Aauth
 *
 * @todo separate (on some level) the unvalidated users from the "banned" users
 */
/**
* 
*/
class MyAauth extends Aauth {
 public function __construct()
 {
  parent::__construct();
 }
 public function get_user_by_email($email)
 {
  $query = $this->aauth_db->where('email', $email);
  $query = $this->aauth_db->get($this->config_vars['users']);
  if ($query->num_rows() <= 0)
  {
   $this->error($this->CI->lang->line('aauth_error_no_user'));
   return FALSE;
  }
  return $query->row();
 }
}
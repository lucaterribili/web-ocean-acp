<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* CodeIgniter Mailjet API V3.1 Class
*
* Displays a Google Map
*
* @package     CodeIgniter
* @subpackage  Libraries
* @category    Libraries
* @author      Mailjet
* @link        https://api.mailjet.com/v3.1/send
* @docs        https://dev.mailjet.com/guides/#send-api-v3-1
*/

class Mailjet
{
	/**
	* The CodeIgniter object variable
	* @access public
	* @var object
	*/
	public $CI;

	/**
	* Variable for loading the config array into
	* @access public
	* @var array
	*/
	public $config_vars;

	########################
	# Base Functions
	########################

	/**
	* Constructor
	*/
	public function __construct()
	{
		// get main CI object
		$this->CI = &get_instance();
		$this->CI->config->load('mailjet');
	}


	########################
	# curl_mailjet Functions
	########################

	//tested
	/**
	 * Call Mailjet Api via Curl
	 * Call Mailjet Api to send transactional email
	 * @param $data
	 * @return json object.
	 */
	public function curl_mailjet($data = null)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->CI->config->item("mj_api_endpoint"));

		// auth
		curl_setopt($ch, CURLOPT_USERPWD, $this->CI->config->item("mj_apikey_public") . ":" . $this->CI->config->item("mj_apikey_private"));
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// headers
		$headers = array($this->CI->config->item('mj_header_content_type'));
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, false);

		// options
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));

		$result = curl_exec($ch);

		curl_close($ch);

		return json_decode($result);
	}


	########################
	# mj_confirm_registration Functions
	########################

	//tested
	/**
	 * Send Email to confirm user registration
	 * Call Mailjet Api to send transactional email by template
	 * @param $to_email
	 * @param $to_name
	 * @param $user_id
	 * @param $secret_code
	 * @return json object
	 */

	public function mj_confirm_registration($to_email, $to_name, $user_id = null, $secret_code = null)
	{
		$data = '{
			"Messages":[
				{
					"From": {
						"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
						"Name": "' . $this->CI->config->item('mj_from_email_noreply') . '"
					},
					"To": [
						{
							"Email": "' . $to_email . '",
							"Name": "' . $to_name . '"
						}
					],
					"TemplateID": ' . $this->CI->config->item('mj_tmp_confirm_registration') . ',
					"TemplateLanguage": true,
					"Subject": "' . $this->CI->config->item('mj_sbj_confirm_registration') . '",
					"Variables": {
						"id": "' . $user_id . '",
						"secret_code": "' . $secret_code . '"
					}
				}
			]
		}';

		return $this->curl_mailjet(($data));
	}


	########################
	# mj_reset_password Functions
	########################

	//tested
	/**
	 * Send Email to reset user password
	 * Call Mailjet Api to send transactional email by template
	 * @param $to_email
	 * @param $to_name
	 * @param $verification_code
	 * @return json object
	 */
	
	public function mj_reset_password($to_email, $to_name, $verification_code = null, $new_password = null)
	{
		$data = '{
			"Messages":[
				{
					"From": {
						"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
						"Name": "' . $this->CI->config->item('mj_from_email_noreply') . '"
					},
					"To": [
						{
							"Email": "' . $to_email . '",
							"Name": "' . $to_name . '"
						}
					],
					"TemplateID": ' . $this->CI->config->item('mj_tmp_reset_password') . ',
					"TemplateLanguage": true,
					"Subject": "' . $this->CI->config->item('mj_sbj_reset_password') . '",
					"Variables": {
						"verification_code": "' . $verification_code . '",
						"new_password": "' . $new_password . '"
					}
				}
			]
		}';

		return $this->curl_mailjet($data);
	}


	########################
	# mj_delete_account_confirm Functions
	########################

	//tested
	/**
	 * Send Email to confirm user account deletion
	 * Call Mailjet Api to send transactional email by template
	 * @param $to_email
	 * @param $to_name
	 * @return json object
	 */
	
	public function mj_delete_account_confirm($to_email, $to_name)
	{
		$data = '{
					"Messages":[
						{
							"From": {
								"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
								"Name": "' . $this->CI->config->item('mj_from_email_noreply') . '"
							},
							"To": [
								{
									"Email": "' . $to_email . '",
									"Name": "' . $to_name . '"
								}
							],
							"TemplateID": ' . $this->CI->config->item('mj_tmp_delete_account_confirm') . ',
							"TemplateLanguage": true,
							"Subject": "' . $this->CI->config->item('mj_sbj_delete_account_confirm') . '",
							"Variables": {}
						}
					]
				}';
		return $this->curl_mailjet($data);
	}


	########################
	# mj_info_request Functions
	########################

	// tested
	/**
	 * Send Email automatically after the user has sent any info request
	 * Call Mailjet Api to send transactional email by template
	 * @param $to_email
	 * @param $to_name
	 * @return json object
	 */

	public function mj_info_request($to_email, $to_name)
	{
		$data = '{
			"Messages":[
				{
					"From": {
						"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
						"Name": "' . $this->CI->config->item('mj_from_email_noreply') . '"
					},
					"To": [
						{
							"Email": "' . $to_email . '",
							"Name": "' . $to_name . '"
						}
					],
					"TemplateID": ' . $this->CI->config->item('mj_tmp_info_request') . ',
					"TemplateLanguage": true,
					"Subject": "' . $this->CI->config->item('mj_sbj_info_request') . '",
					"Variables": {
						"sender_name" : "' . $to_name . '"
					}
				}
			]
		}';

		return $this->curl_mailjet(($data));
	}

	// mj_customer_emails
	public function mj_customer_msg($config)
	{
		$data = '{
			"Messages":[
				{
					"From": {
						"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
						"Name": "' . $config['from_name'] . '"
					},
					"To": [
						{
							"Email": "' . $config['to_email'] . '",
							"Name": "' . $config['to_name'] . '"
						}
					],
					"ReplyTo": {
						"Email": "' . $config['from_email'] . '",
						"Name": "'  . $config['from_name'] . '"
					},
					"TemplateID": ' . $this->CI->config->item('mj_tmp_customer_msg') . ',
					"TemplateLanguage": true,
					"Subject": "' . $this->CI->config->item('mj_sbj_info_msg') . '",
					"Variables": {
						"full_name" : "' .      $config['from_name'] . '",
						"msg" : "' .            $config['msg'] . '"
					}
				}
			]
		}';

		return $this->curl_mailjet(($data));
	}

	public function mj_customer_confirm($config)
	{
		$data = '{
			"Messages":[
				{
					"From": {
						"Email": "' . $this->CI->config->item('mj_from_email_noreply') . '",
						"Name": "' . $config['from_name'] . '"
					},
					"To": [
						{
							"Email": "' . $config['to_email'] . '",
							"Name": "' . $config['to_name'] . '"
						}
					],
					"TemplateID": ' . $this->CI->config->item('mj_tmp_customer_confirm') . ',
					"TemplateLanguage": true,
					"Subject": "' . $this->CI->config->item('mj_sbj_info_request') . '",
					"Variables": {
						"full_name" : "' .      $config['to_name'] . '",
						"activity" : "' .       $config['from_name'] . '"
					}
				}
			]
		}';

		return $this->curl_mailjet(($data));
	}
}

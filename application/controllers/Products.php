<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->config('ecommerce');

	}

    public function index(){
      $global = [
                 'uri' => 'dashboard'
                ];
      $this->global = array_merge($this->global, $global);
      $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
      $this->load->view($this->config->item('site_theme') . '/views/dashboard');
      $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
    }

    public function all_categories(){
      $global = [
                 'uri' => 'list'
                ];
      $cats = api_main_bg_categories();

      $this->global = array_merge($this->global, $global);
      $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
      $this->load->view($this->config->item('site_theme') . '/views/list-woo-categories', $cats);
      $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
    }

    public function all_products(){
      $global = [
                 'uri' => 'list'
                ];

      $products = api_all_bg_products();
      
      $this->global = array_merge($this->global, $global);
      $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
      $this->load->view($this->config->item('site_theme') . '/views/list-woo-products', $products);
      $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
    }

// utility importer and exporter

    public function import_cats_from_big_buy(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_categories());
    }

    public function import_bg_categories_tech(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_categories_tech());
    }

   public function import_bg_categories_kitchen(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_categories_kitchen());
    }

    public function import_products_from_big_buy(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_products());  
    }

    public function import_bg_products_images(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_products_images());  
    }

    public function import_bg_products_informations(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_products_informations());  
    }

    public function import_bg_products_stocks(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_products_stocks());  
    }
    
    public function import_bg_products_attributes(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_import_bg_products_attributes());  
    }


    // woocoomerce import
    public function export_woo_categories(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_export_woo_categories());
    }

    public function export_woo_products(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_export_woo_products());
    }

    public function export_woo_images(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_export_woo_images());
    }

    public function export_woo_extra_fields(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_export_woo_extra_fields());
    }

    public function export_woo_brands(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_export_woo_brands());
    }

    public function export_woo_products_tags(){
        $this->output->set_content_type('application/json');
        echo json_encode(export_woo_products_tags());
    }

    public function order_woo_categories(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_order_woo_categories());
    }
    // update
    public function update_woo_category($id){
        $this->output->set_content_type('application/json');
        echo json_encode(api_update_woo_category(['term_id' => $id] ));
    }

    public function update_woo_product($sku){
        $this->output->set_content_type('application/json');
        echo json_encode(api_update_woo_product(['sku' => $sku]));
    }

    public function update_woo_products(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_update_woo_products());
    }

    public function update_big_buy_stocks(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_update_bg_products_stocks());
    }

    public function update_woo_store(){
        $this->output->set_content_type('application/json');
        echo json_encode(api_manage_stock_woo_store());
    }

    // all 

    public function import_from_big_buy(){

        $this->output->set_content_type('application/json');

        $cats = api_import_bg_categories();
        if(is_null($cats)){
            $data = ['status' => FALSE, 'message' => 'errore importazione categorie'];
            echo json_encode($data);
            return false;
        }
        
        $products = api_import_bg_products();
        if(is_null($products)){
            $data = ['status' => FALSE, 'message' => 'errore importazione prodotti'];
            echo json_encode($data);
            return false;
        }

        $images = api_import_bg_products_images();
        if(is_null($images)){
            $data = ['status' => FALSE, 'message' => 'errore importazione immagini'];
            echo json_encode($data);
            return false;
        }

        $info = api_import_bg_products_informations();
        if(is_null($info)){
            $data = ['status' => FALSE, 'message' => 'errore importazione info'];
            echo json_encode($data);
            return false;
        }

        $stocks = api_import_bg_products_stocks();
        if(is_null($stocks)){
            $data = ['status' => FALSE, 'message' => 'errore importazione stocks'];
            echo json_encode($data);
            return false;
        }

        $tech = api_import_bg_products_tech();
        if(is_null($tech)){
            $data = ['status' => FALSE, 'message' => 'errore importazione stocks'];
            echo json_encode($data);
            return false;
        }

         $data = ['status' => TRUE, 'message' => 'Tutti i dati sono stati importati'];
         echo json_encode($data);  

    }
    
    public function export_to_woocommerce(){
        $this->output->set_content_type('application/json');

        $cats = json_encode(api_export_woo_categories());
        if(is_null($cats)){
            $data = ['status' => FALSE, 'message' => 'errore esportazione categorie'];
            echo json_encode($data);
            return false;
        }

        $products = json_encode(api_export_woo_products());
        if(is_null($products)){
            $data = ['status' => FALSE, 'message' => 'errore esportazione prodotti'];
            echo json_encode($data);
            return false;
        }
        
        $order = json_encode(api_order_woo_categories());
        if(is_null($order)){
            $data = ['status' => FALSE, 'message' => 'errore ordine categorie'];
            echo json_encode($data);
            return false;
        }

          $data = ['status' => TRUE, 'message' => 'Tutti i dati sono stati importati su WooCommerce'];
         echo json_encode($data);  

    }
    
}
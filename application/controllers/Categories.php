<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
	  $filter = $this->input->post('filter', '');
	  $field  = $this->input->post('field', '');
	  $global = [
	  			 'uri' => 'list'
	  			];
	  // richiama api
	  $categories = api_all_activities($filter, $field);

      $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-categories', $categories);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function add(){
	  $global = [
	  			 'uri' => 'form'
	  			 ];
	  $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-categories');
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function update(){
	  $global = [
	  			 'uri' => 'form'
	  			];		
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-categories');
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	// controllers actions

	public function send_add(){
		var_dump($this->input->post());
	}

	public function bulk_delete()
	{
	 $send_data = [
	 				'ids' => $this->input->post('id')
	              ];
	 $data = api_delete_bulk_activity($send_data);
	 var_dump($data);
	}

	public function suggest($filter){
		$this->output->set_content_type('application/json');
		echo json_encode(api_get_subcategory_list($filter));		
	}
}
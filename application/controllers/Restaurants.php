<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants extends MY_Controller {

	public function __construct(){
		parent::__construct();
		check_login();
		check_admin();
	}

	public function display_group(){
	  $global = [
	  			 'uri' => 'list'
	  			];
	  $this->global = array_merge($this->global, $global);
	  $data = api_restaurants_group_display();

	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-restaurant-group', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function display_items(){
	  $global = [
	  			 'uri' => 'list'
	  			];
	  			
	  $this->global = array_merge($this->global, $global);	  
	  $data = api_restaurants_items_display();

	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-restaurant-items', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function add_group(){
	  $global = [
	  			 'uri' => 'simple-form'
	  			 ];
	  $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-restaurant-group');
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function add_items(){
	  $global = [
	  			 'uri' => 'simple-form'
	  			 ];
	  $this->global = array_merge($this->global, $global);

	  $data = api_restaurants_group_display();
	  
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);

	  $this->load->view($this->config->item('site_theme') . '/views/form-restaurant-items', $data);
	  
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function update_group($id){
	  $global = [
	  			 'uri' => 'edit-form'
	  			];
	  $data = api_get_activity_detail($id)->data;

	  $this->global = array_merge($this->global, $global);			
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-items-update', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	// controllers actions

	public function send_add_group(){
		// push data for complete add restaurant dish
		$save_data['label'] = $this->input->post('label');
		// call api
		$data = api_restaurants_group_add($save_data); 
		$this->output->set_content_type('application/json');
		echo json_encode($data);
	}

	public function send_add_items(){
		$media = '';
		if(!is_null($_FILES['media'])){
			$media = base64_encode(file_get_contents($_FILES['media']['tmp_name']));
		}
		$save_data = [
				      'item_fk' => $this->input->post('item_fk'),
				      'rg_fk' => $this->input->post('rg_fk'),
				      'title' => $this->input->post('title'),
				      'media' => $media,
				      'description' => $this->input->post('description'),
				      'price' => $this->input->post('price')      
					 ];
		// call api
		$data = api_restaurants_items_add($save_data); 
		$this->output->set_content_type('application/json');
		echo json_encode($data);
	}

	public function send_update(){
		
	}

	public function bulk_delete()
	{
	 $send_data = [
	 				'ids' => $this->input->post('id')
	              ];
	 $data = api_delete_bulk_activity($send_data);
	 var_dump($data);
	}
}
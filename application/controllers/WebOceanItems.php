<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WebOceanItems extends MY_Controller {

	public function __construct(){
		parent::__construct();
		check_login();
		check_admin();
	}
    
    public function suggest($string){
		$data = api_webocean_items_suggest($string); 
		$this->output->set_content_type('application/json');
		echo json_encode($data);    		
    }
}
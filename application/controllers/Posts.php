<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends MY_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index()
	{
	  $global = [
	  			 'uri' => 'list'
	  			];
	  $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list');
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}
}
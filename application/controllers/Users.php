<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct(){
		parent::__construct();
		check_login();
		check_admin();

	}

	public function display_list_one(){
	// WEB OCEAN USERS
	  $global = [
	  			 'uri' => 'list'
	  			];
	  $this->global = array_merge($this->global, $global);
	  // get data from api
	  $data = api_users_webocean();

	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-users', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function display_list_two(){
		// OSTIA TV USERS
	  $global = [
	  			 'uri' => 'list'
	  			];
	  $this->global = array_merge($this->global, $global);
	  // get data from api
	  $data = api_user_all(2, 'aauth_users_profile.from_app_fk');	  
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-users', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);		
	}
}
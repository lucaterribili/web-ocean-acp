<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function login(){
		if($this->account->is_loggedin()){
		 redirect('/');
		}
		$this->load->view($this->config->item('site_theme') . '/views/login', $this->global);
	}

	public function logout(){
		$this->account->logout();
		redirect(BASE_URL);
	}

	// send data

	public function send_login(){
    	$credentials = [
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
		];

		$api    = $this->config->item('api_gateway').'api/user/login';
		$result = api_login_user($credentials);

		if ($result->status == true) {

			$this->account->set_jwt_session($result, $result->token);
		}
		echo json_encode($result);		
	}
}

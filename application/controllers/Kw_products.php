<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kw_products extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('woocommerce');

	}

	public function index()
	{
	  $filter = $this->input->post('filter', '');
	  $field  = $this->input->post('field', '');
	  $global = [
	  			 'uri' => 'list'
	  			];
	  // richiama api
	  /*
	  $items = api_all_activities($filter, $field);

      $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/list-items', $items);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	  */
	}

	public function test(){
		$this->woo = $this->woocommerce->request();

		print_r( $this->woo->products->create( array('product_id' => 121, 'title' => 'Test Product 121', 'type' => 'simple', 'regular_price' => '9.99', 'description' => 'test', 	'images' => [
		[
			'src'      => 'https://www.google.it/images/branding/googlelogo/2x/googlelogo_color_92x30dp.png',
			'position' => 0,
		]
	] ) ) );
}
	public function add(){
	  $global = [
	  			 'uri' => 'form'
	  			 ];
	  $this->global = array_merge($this->global, $global);
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-items');
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	public function update($id){
	  $global = [
	  			 'uri' => 'edit-form'
	  			];
	  $data = api_get_activity_detail($id)->data;

	  $this->global = array_merge($this->global, $global);			
	  $this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
	  $this->load->view($this->config->item('site_theme') . '/views/form-items-update', $data);
	  $this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}

	// controllers actions

	public function send_add(){
		$save_data = $this->input->post();
		// push data for complete add activity
		$save_data['provider'] = $this->config->item('site_provider');
		// push data for add_website
		$save_data['user_fk'] = $this->user->id;
		$save_data['template_fk'] = 1;
		$save_data['domain'] = '';
		$save_data['https'] = 1;
		// call api
		$data = api_generate_activity($save_data); 
   		if($data->status == TRUE){
			$dp_data = [
				'dp_id'			=> $data->id,
				'user_fk'       => $this->user->id,
				'has_post'		=> 0,
				'has_website'	=> 0
			];
			$result = api_enable_post_website($dp_data);
		}
		$this->output->set_content_type('application/json');
		echo json_encode($data);
	}

	public function send_update(){
		
	}

	public function bulk_delete()
	{
	 $send_data = [
	 				'ids' => $this->input->post('id')
	              ];
	 $data = api_delete_bulk_activity($send_data);
	 var_dump($data);
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acp extends MY_Controller {

	public function __construct(){
		parent::__construct();
		if(!$this->account->is_loggedin() && $this->uri->segment(2) != 'login')
     	{
      
      		redirect('auth/login');
     
    	}

	}

	public function index()
	{
	  	$global = [
	  			 'uri' => 'index'
	  			  ];
	  	$this->global = array_merge($this->global, $global);		
		$this->load->view($this->config->item('site_theme') . '/views/header', $this->global);
		$this->load->view($this->config->item('site_theme') . '/views/index');
		$this->load->view($this->config->item('site_theme') . '/views/footer', $this->global);
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

// SITE
$config['site_metatitle']		= 'Acp Red Velvet';
$config['site_provider']		= 'yx';
$config['site_description']		= 'Tutto si controlla da qui';
$config['app_fk']				= 2;
$config['info_email']			= 'info@yourindex.red';
$config['deploy']               = TRUE;
	
// FILE SERVER	
$config['old_static_repo']		= 'https://static.yourindex.red/';
$config['static_repo']			= 'https://static.yourindex.red/uploads/';
// API GATEWAY	
//$config['api_gateway']			= 'http://localhost/restserver/api/';
switch ($_SERVER['HTTP_HOST']) {
	case 'localhost':
	case 'acp.webocean.test':
		$config['api_gateway'] = 'http://localhost/webocean/restserver/api/';
		$config['site_domain'] = 'localhost';
	break;
	default:
		$config['api_gateway'] = 'https://rest.webocean.it/api/';
		$config['site_domain'] = 'collaudo.yourindex.red';
	break;
}
// API KEY
$config['x_api_key']			= 'X-Api-Key: 29449C0468269849E10A862BE17A463F';
//$config['x_api_key']			= 'X-Api-Key: C6AADADE98644D2BC5F83453E311B785';
$config['google_maps_key']		= 'AIzaSyBwsxcQzYQt8ombysLtqbaJVPxLB8oOA4U';
$config['google_recaptcha_key'] = '6LfERH4UAAAAAEQQVRoKHNTNtZwG8ImCdnL4sm1B';
// BIG BUY GATEWAY
$config['bigbuy_sandbox'] = TRUE;
if ($config['bigbuy_sandbox'] == TRUE) {
	$config['bigbuy_gateway'] = 'https://api.sandbox.bigbuy.eu';
	$config['bigbuy_api_key'] = 'Authorization: Bearer YzgwM2Y2MTE4MGIwOTcyMzExMjAxY2NkZTRjNWIyZGNlMDgwYTVmYzA4Mzc4M2E3MDkyMjMxNWFjYzI4OTAzMA';
}
else{
	$config['bigbuy_gateway'] = 'https://api.bigbuy.eu';
	$config['bigbuy_api_key'] = 'Bearer YzgwM2Y2MTE4MGIwOTcyMzExMjAxY2NkZTRjNWIyZGNlMDgwYTVmYzA4Mzc4M2E3MDkyMjMxNWFjYzI4OTAzMA';
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Mailjet Config
| -------------------------------------------------------------------
| A Mailjet wrapper library for CodeIgniter 3.x
|
| -------------------------------------------------------------------
| EXPLANATION
| -------------------------------------------------------------------
|
|   ['mj_api_endpoint']						Api Endpoint (V. 3.1)
|	['mj_apikey_public']					Public Api Key (Username) 
|   ['mj_apikey_private']					Private Api Key (Password)
|
|	['mj_header_content_type']				Default Header Content-Type - application/json
|
|   ['mj_tmp_confirm_registration']			Template ID of "yix_conferma_registrazione"
|   ['mj_tmp_reset_password']       		Template ID of "yix_reimposta_password"
|   ['mj_tmp_delete_account_confirm'] 		Template ID of "yix_cancellazione_account"
|
|   ['mj_sbj_confirm_registration']			Subject of "Conferma la tua registrazione"
|   ['mj_sbj_reset_password']       		Subject of "Reimposta la tua password"
|   ['mj_sbj_delete_account_confirm'] 		Subject of "Conferma cancellazione account"
|
|	['mj_from_email_noreply']				Default "no-reply" account - no-reply@domain";
|	['mj_from_name_noreply']				Default "support" account - support@domain";
|
|	['mj_from_email_support']="support@yourindex.red";
|	['mj_from_name_support']="support";
*/

$config['mj_api_endpoint']="https://api.mailjet.com/v3.1/send";
$config['mj_apikey_public']="00ee063491e1bf5253ecedf3963d565e";
$config['mj_apikey_private']="0cba8120f810b0b417aed7d65395da9c";

$config['mj_header_content_type']='Content-Type: application/json';

$config['mj_tmp_confirm_registration']=503123;
$config['mj_tmp_reset_password']=503154;
$config['mj_tmp_delete_account_confirm']=503146;
$config['mj_tmp_info_request']=511645;
// model for customers
$config['mj_tmp_customer_confirm']=544663;
$config['mj_tmp_customer_msg']=544618;
// other
$config['mj_sbj_confirm_registration']="Conferma la tua registrazione";
$config['mj_sbj_reset_password']="Reimposta la tua password";
$config['mj_sbj_delete_account_confirm']="Conferma cancellazione account";
$config['mj_sbj_info_request']='Grazie per averci contattato';
$config['mj_sbj_info_msg']='Richiesta d\'informazioni';

$config['mj_from_email_noreply']="no-reply@yourindex.red";
$config['mj_from_name_noreply']="no-reply";

$config['mj_from_email_support']="support@yourindex.red";
$config['mj_from_name_support']="support";
